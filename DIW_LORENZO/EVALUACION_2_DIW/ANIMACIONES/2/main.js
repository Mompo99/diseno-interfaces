// Aqui con id

let cuadrado = document.getElementById("cuadrado");
cuadrado.onmouseover = function () {
  document.getElementById("cuadrado").style.fontSize = '30px';
};
cuadrado.onmouseout = function () {
  document.getElementById("cuadrado").style.fontSize = '12px';
};

// Aqui con class

let cuadrado = document.getElementsByClassName("c-cuadrado");
console.log(cuadrado);
console.log(cuadrado[0]);
let texto = cuadrado[0];

texto.onmouseover = function () {
  texto.style.fontSize = '30px';
};
texto.onmouseout = function () {
  texto.style.fontSize = '12px';
};